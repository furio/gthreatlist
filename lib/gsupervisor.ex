defmodule Gthreatlist.Supervisor do
    use Supervisor
    
    def start_link(name) do
        Supervisor.start_link(__MODULE__, :ok, name: name)
    end
    
    def init(:ok) do
        children = [
            worker(Gthreatlist.Worker, [Gthreatlist.Worker])
        ]
        
        supervise(children, strategy: :rest_for_one)
    end
end