defmodule Gthreatlist do
  use Application

  def start(_type, _args) do
    Gthreatlist.Supervisor.start_link
  end
end
